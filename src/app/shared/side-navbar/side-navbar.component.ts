import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { IMenu } from 'src/app/interface/IMenu';
import { HttpclientService } from 'src/app/services/HttpClientServices';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-side-navbar',
  templateUrl: './side-navbar.component.html',
  styleUrls: ['./side-navbar.component.scss']
})
export class SideNavbarComponent implements OnInit {

  private apiURL: string = 'http://localhost:4200/assets/menu.json';

  menuList: IMenu[];

  constructor(
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.http.get<IMenu[]>(`${this.apiURL}`)
    .subscribe((menus) => {
      this.menuList = menus;
    });
  }

}
