import { PersonalInformation } from '../interface/IPersonalInformation';
import { Avatar } from '../interface/IAvatar';
import { ICompany } from '../interface/ICompany';
import { IRole } from '../interface/IRole';

export class User {
  id?: number;
  personalInformation: PersonalInformation;
  username: string;
  password: string;
  receiveEmails: boolean;
  status: string;
  roles: IRole[];
  avatar: Avatar;
  companyList: ICompany[];

  constructor(item: any) {
    this.id = item?.id;
    this.personalInformation = {
      id: item?.personalInformation.id,
      firstName: item.personalInformation.firstName,
      lastName: item.personalInformation.lastName,
      email: item.personalInformation.email,
      activated: item.personalInformation.activated
    }
    this.username = item.username;
    this.password = item.password;
    this.receiveEmails = item.receiveEmails;
    this.status = 'PENDING';
    this.roles = item.roles;
    this.avatar = {
      id: item?.id,
      avatar220: item.avatar
    };
    this.companyList = item.companyList;
  }
}
