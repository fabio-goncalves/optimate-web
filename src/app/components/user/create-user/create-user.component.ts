import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { CompanyService } from '../../../services/company.service';
import { StorageService } from '../../../services/storage.service';
import { Component, OnDestroy, OnInit, Optional } from '@angular/core';
import {
  AbstractControlOptions,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CustomValidators } from 'src/app/config/CustomValidators';
import { ICompany } from 'src/app/interface/ICompany';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/services/user.service';
import { AlertComponent } from 'src/app/shared/alert/alert.component';
import { RoleService } from 'src/app/services/role.service';
import { IRole } from 'src/app/interface/IRole';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss'],
})
export class CreateUserComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  user: User;
  dialogConfig: MatDialogConfig = new MatDialogConfig();
  errorMessage = '';
  subscription: Subscription;
  companyList: ICompany[] = [];
  roleList: IRole[] = [];


  constructor(
    private storageService: StorageService,
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private companyService: CompanyService,
    private roleService: RoleService,
    private dialog: MatDialog,
    @Optional() private dialogRef: MatDialogRef<CreateUserComponent>
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group(
      {
        personalInformation: this.formBuilder.group({
          firstName: ['', Validators.compose([Validators.required])],
          lastName: ['', Validators.compose([Validators.required])],
          email: [
            '',
            Validators.compose([
              Validators.required,
              Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/),
            ]),
          ],
          activated: ['true'],
        }),
        username: [
          '',
          Validators.compose([Validators.required, Validators.minLength(6)]),
        ],
        password: [
          '',
          Validators.compose([
            Validators.required,
            Validators.pattern(
              /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/
            ),
          ]),
        ],
        confirmPassword: ['', Validators.compose([Validators.required])],
        receiveEmails: ['true'],
        status: [''],
        roles: [''],
        avatar: [''],
        companyList: ['', Validators.compose([Validators.required])],
      },
      {
        validator: CustomValidators.MatchingPasswords,
      } as AbstractControlOptions
    );
    this.listCompanies();
    this.listRoles();
  }

  createUser() {
    if (this.form.valid) {
      this.user = new User(this.form.value);
      this.user.companyList = this.companyList;
      this.user.roles = this.roleList;
      this.subscription = this.userService.createUser(this.user).subscribe({
        next: (data) => {
          let id = data?.id;
          this.dialogRef.close(true);
          this.router.navigate(['/list-user']);
        },
        error: (err) => {
          this.errorMessage = err.error.message;
          this.showMessage(this.errorMessage);
          if (err.status == '401') {
            this.storageService.logout();
            this.router.navigate(['/login']);
          }
        },
      });
    }
  }

  cancel() {
    this.form.controls['username'].setErrors({ incorrect: true });
    if(this.dialogRef)
      this.dialogRef.close(false);
  }

  enableButton(): string {
    if (this.form.valid) {
      return 'button';
    } else {
      return 'disableButton';
    }
  }

  uploadAvatar(id: any): void {
    this.user.id = id;
    this.subscription = this.userService.uploadAvatar(this.user).subscribe({
      next: (data) => {},
    });
  }

  listCompanies():void {
    this.subscription = this.companyService.listAll().subscribe({
      next: (data) => {
        this.companyList = data;
      },
      error: (err) => {
        this.errorMessage = err.error.message;
        this.showMessage(this.errorMessage);
      }
    });
  }

  listRoles():void {
    this.subscription = this.roleService.listAll().subscribe({
      next: (data) => {
        this.roleList = data;
      },
      error: (err) => {
        this.errorMessage = err.error.message;
        this.showMessage(this.errorMessage);
      }
    });
  }

  showMessage(message: string):void {
    this.dialogConfig.disableClose = false;
    this.dialogConfig.enterAnimationDuration = '500ms';
    this.dialogConfig.exitAnimationDuration = '500ms';
    this.dialogConfig.data = {
      messageAlert: message
    };

    this.dialog.open(AlertComponent, this.dialogConfig);
  }

  ngOnDestroy(): void {
    if (this.subscription) this.subscription.unsubscribe();
  }
}
