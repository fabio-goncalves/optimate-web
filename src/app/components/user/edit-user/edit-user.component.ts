import { PersonalInformation } from '../../../interface/IPersonalInformation';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControlOptions,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CustomValidators } from 'src/app/config/CustomValidators';
import { ICompany } from 'src/app/interface/ICompany';
import { User } from 'src/app/model/user';
import { CompanyService } from 'src/app/services/company.service';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';
import { AlertComponent } from 'src/app/shared/alert/alert.component';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
})
export class EditUserComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  subscription: Subscription;
  user: User;
  companyList: ICompany[] = [];
  dialogConfig: MatDialogConfig = new MatDialogConfig();
  errorMessage = '';

  constructor(
    private storageService: StorageService,
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private companyService: CompanyService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<EditUserComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.user = data.user;
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group(
      {
        id: [this.user.id],
        personalInformation: this.formBuilder.group({
          id: [this.user.personalInformation.id],
          firstName: [
            this.user.personalInformation.firstName,
            Validators.compose([Validators.required]),
          ],
          lastName: [
            this.user.personalInformation.lastName,
            Validators.compose([Validators.required]),
          ],
          email: [
            this.user.personalInformation.email,
            Validators.compose([
              Validators.required,
              Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/),
            ]),
          ],
          activated: [this.user.personalInformation.activated],
        }),
        username: [
          this.user.username,
          Validators.compose([Validators.required, Validators.minLength(6)]),
        ],
        receiveEmails: [this.user.receiveEmails],
        status: [this.user.status],
        roles: [''],
        avatar: [this.user.avatar.avatar220],
        companyList: [
          this.user.companyList,
          Validators.compose([Validators.required]),
        ],
      },
      {
        validator: CustomValidators.MatchingPasswords,
      } as AbstractControlOptions
    );
    this.subscription = this.companyService.listAll().subscribe({
      next: (data) => {
        this.companyList = this.user.companyList.length
          ? this.user.companyList
          : data;
      },
    });
  }

  editUser(): void {
    if (this.form.valid) {
      this.user = new User(this.form.value);
      this.user.companyList = this.companyList;
      this.subscription = this.userService.editUser(this.user).subscribe({
        next: (data) => {
          let id = data?.id;
          this.uploadAvatar(id);
          this.dialogRef.close(true);
        },
        error: (err) => {
          this.errorMessage = err.error.message;
          this.showMessage(this.errorMessage);
          if (err.status == '401') {
            this.storageService.logout();
            this.router.navigate(['/login']);
          }
        },
      });
    }
  }

  uploadAvatar(id: any): void {
    this.user.id = id;
    this.subscription = this.userService.uploadAvatar(this.user).subscribe({
      next: () => {},
    });
  }

  cancel(): void {
    this.form.controls['username'].setErrors({ incorrect: true });
    this.dialogRef.close(false);
  }

  enableButton(): string {
    if (this.form.valid) {
      return 'button';
    } else {
      return 'disableButton';
    }
  }
  showMessage(message: string):void {
    this.dialogConfig.disableClose = false;
    this.dialogConfig.enterAnimationDuration = '500ms';
    this.dialogConfig.exitAnimationDuration = '500ms';
    this.dialogConfig.data = {
      messageAlert: message
    };

    this.dialog.open(AlertComponent, this.dialogConfig);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
