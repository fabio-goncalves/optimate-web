import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { StorageService } from '../../../services/storage.service';
import { Component, Inject, OnDestroy, OnInit, Optional } from '@angular/core';
import {
  AbstractControlOptions,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CustomValidators } from 'src/app/config/CustomValidators';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/services/user.service';
import { AlertComponent } from 'src/app/shared/alert/alert.component';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  user: any;
  currentPassword: string;
  id: number;
  dialogConfig: MatDialogConfig = new MatDialogConfig();
  errorMessage = '';
  subscription: Subscription;

  constructor(
    private storageService: StorageService,
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private dialog: MatDialog,
    @Optional() private dialogRef: MatDialogRef<ChangePasswordComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.id = data.user.id;
    this.currentPassword = data.user.password;
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group(
      {
        password: [
          '',
          Validators.compose([
            Validators.required,
            Validators.pattern(
              /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/
            ),
          ]),
        ],
        confirmPassword: ['', Validators.compose([Validators.required])],
      },
      {
        validator: CustomValidators.MatchingPasswords,
      } as AbstractControlOptions
    );
  }

  changePassword(): void {
    if (this.form.valid) {
      this.user = this.form.value;
      this.subscription = this.userService
        .changePassword(this.id, this.currentPassword, this.user.password)
        .subscribe({
          next: () => {
            this.dialogRef.close(true);
          },
          error: (err) => {
            this.errorMessage = err.error.message;
            this.showMessage(this.errorMessage);
            if (err.status == '401') {
              this.storageService.logout();
              this.router.navigate(['/login']);
            }
          },
        });
    }
  }

  cancel() {
    this.form.controls['password'].setErrors({ incorrect: true });
    this.dialogRef.close(false);
  }

  enableButton(): string {
    if (this.form.valid) {
      return 'button';
    } else {
      return 'disableButton';
    }
  }

  showMessage(message: string):void {
    this.dialogConfig.disableClose = false;
    this.dialogConfig.enterAnimationDuration = '500ms';
    this.dialogConfig.exitAnimationDuration = '500ms';
    this.dialogConfig.data = {
      messageAlert: message
    };

    this.dialog.open(AlertComponent, this.dialogConfig);
  }

  ngOnDestroy(): void {
    if (this.subscription) this.subscription.unsubscribe();
  }
}
