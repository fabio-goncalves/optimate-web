import { StorageService } from '../../../services/storage.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ICompany } from 'src/app/interface/ICompany';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { EventBusService } from 'src/app/shared/event-bus.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { AlertComponent } from 'src/app/shared/alert/alert.component';

@Component({
  selector: 'app-profile-user',
  templateUrl: './profile-user.component.html',
  styleUrls: ['./profile-user.component.scss'],
})
export class ProfileUserComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  user: User;
  errorMessage = '';
  subscription?: Subscription;
  companyList: ICompany[] = [];
  mobileQuery: MediaQueryList;
  currentUser: any;
  isLoggedIn = false;
  dialogConfig: MatDialogConfig = new MatDialogConfig();

  constructor(
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router,
    private userService: UserService,
    private eventBusService: EventBusService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();
    if (this.isLoggedIn) {
      this.currentUser = this.storageService.getUser();
    } else {
      this.logout();
    }
    this.subscription = this.eventBusService.on('logout', () => {
      this.logout();
    });
    this.subscription = this.userService
      .findUserById(this.currentUser.id)
      .subscribe({
        next: (data) => {
          this.user = data;
        },
        error: err => {
          this.errorMessage = err.error.message;
          this.showMessage(this.errorMessage);
        }
      });
  }

  editUser(user: User): void {
    this.dialogConfig.disableClose = true;
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.width = '800px';
    this.dialogConfig.height = '650px';
    this.dialogConfig.enterAnimationDuration = '500ms';
    this.dialogConfig.exitAnimationDuration = '500ms';

    this.dialogConfig.data = {
      user: user,
    };

    this.dialog.open(EditUserComponent, this.dialogConfig);
  }

  changePassword(user: User): void {
    this.dialogConfig.disableClose = false;
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.width = '700px';
    this.dialogConfig.height = '350px';
    this.dialogConfig.enterAnimationDuration = '500ms';
    this.dialogConfig.exitAnimationDuration = '500ms';

    this.dialogConfig.data = {
      user: user,
    };

    this.dialog.open(ChangePasswordComponent, this.dialogConfig);
  }

  logout(): void {
    this.authService.logout().subscribe({
      next: () => {
        this.storageService.logout();
        this.router.navigate(['/login']);
      },
    });
  }

  showMessage(message: string):void {
    this.dialogConfig.disableClose = false;
    this.dialogConfig.enterAnimationDuration = '500ms';
    this.dialogConfig.exitAnimationDuration = '500ms';
    this.dialogConfig.data = {
      messageAlert: message
    };

    this.dialog.open(AlertComponent, this.dialogConfig);
  }

  ngOnDestroy(): void {
    if (this.subscription) this.subscription.unsubscribe();
  }
}
