import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { RoleService } from 'src/app/services/role.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss'],
})
export class RoleComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  subscription: Subscription;
  role: string;

  constructor(
    private formBuilder: FormBuilder,
    private roleService: RoleService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      role: ['', Validators.minLength(6)],
    });
  }

  addRole(): void {
    if (this.form.valid) {
      this.subscription = this.roleService.addRole(this.role).subscribe({
        next: (data) => {
          // let id = data?.id;
          // this.dialogRef.close(true);
          // this.router.navigate(['/list-user']);
        },
        error: (err) => {
          // this.errorMessage = err.error.message;
          // this.showMessage(this.errorMessage);
          if (err.status == '401') {
            // this.storageService.logout();
            // this.router.navigate(['/login']);
          }
        },
      });
    }
  }

  ngOnDestroy(): void {}
}
