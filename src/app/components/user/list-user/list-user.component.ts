import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { IUser } from 'src/app/interface/IUser';
import { UserService } from 'src/app/services/user.service';
import { CreateUserComponent } from '../create-user/create-user.component';
import { DeleteUserComponent } from '../delete-user/delete-user.component';
import { ViewUserComponent } from '../view-user/view-user.component';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import { Router } from '@angular/router';
import { EventBusService } from 'src/app/shared/event-bus.service';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss'],
})
export class ListUserComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['id', 'Username', 'Name', 'Role', 'Status', 'Action'];
  dataSource: MatTableDataSource<IUser>;
  usersSubscription: Subscription = new Subscription();
  subscription?: Subscription;
  dialogConfig: MatDialogConfig = new MatDialogConfig();
  isLoggedIn = false;
  currentUser: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private userDervice: UserService,
    private dialog: MatDialog,
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router,
    private eventBusService: EventBusService
  ) {}

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();
    if (this.isLoggedIn) {
      this.currentUser = this.storageService.getUser();
    } else {
      this.logout();
    }
    this.subscription = this.eventBusService.on('logout', () => {
      this.logout();
    });
    this.userDervice.listAll();
    this.usersSubscription = this.userDervice.users$.subscribe({
      next: (users) => {
        this.dataSource = new MatTableDataSource(users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addUser(): void {
    this.dialogConfig.disableClose = true;
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.width = '1080px';
    this.dialogConfig.height = '650px';
    this.dialogConfig.enterAnimationDuration = '500ms';
    this.dialogConfig.exitAnimationDuration = '500ms';

    this.dialog
      .open(CreateUserComponent, this.dialogConfig)
      .afterClosed()
      .subscribe((shouldReload: boolean) => {
        if (shouldReload) this.userDervice.listAll();
      });
  }

  viewUser(user: IUser): void {

    this.dialogConfig.disableClose = false;
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.width = '800px';
    this.dialogConfig.height = '650px';
    this.dialogConfig.enterAnimationDuration = '500ms';
    this.dialogConfig.exitAnimationDuration = '500ms';

    this.dialogConfig.data = {
      user: user,
    };

    this.dialog
      .open(ViewUserComponent, this.dialogConfig)
      .afterClosed()
      .subscribe((shouldReload: boolean) => {
        if (shouldReload) this.userDervice.listAll();
      });
  }

  editUser(user: IUser): void {
    this.dialogConfig.disableClose = true;
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.width = '800px';
    this.dialogConfig.height = '650px';
    this.dialogConfig.enterAnimationDuration = '500ms';
    this.dialogConfig.exitAnimationDuration = '500ms';

    this.dialogConfig.data = {
      user: user,
    };

    this.dialog
      .open(EditUserComponent, this.dialogConfig)
      .afterClosed()
      .subscribe((shouldReload: boolean) => {
        if (shouldReload) this.userDervice.listAll();
      });
  }

  changePassword(user: IUser): void {
    this.dialogConfig.disableClose = false;
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.width = '700px';
    this.dialogConfig.height = '350px';
    this.dialogConfig.enterAnimationDuration = '500ms';
    this.dialogConfig.exitAnimationDuration = '500ms';

    this.dialogConfig.data = {
      user: user,
    };

    this.dialog
      .open(ChangePasswordComponent, this.dialogConfig)
      .afterClosed()
      .subscribe((shouldReload: boolean) => {
        if (shouldReload) this.userDervice.listAll();
      });
  }

  deleteUser(user: IUser): void {
    this.dialogConfig.disableClose = false;
    this.dialogConfig.width = '350px';
    this.dialogConfig.height = '200px';
    this.dialogConfig.enterAnimationDuration = '500ms';
    this.dialogConfig.exitAnimationDuration = '500ms';
    this.dialogConfig.data = {
      id: user.id,
      username: user.username,
    };
    this.dialog
      .open(DeleteUserComponent, this.dialogConfig)
      .afterClosed()
      .subscribe((shouldReload: boolean) => {
        if (shouldReload) this.userDervice.listAll();
      });
  }

  statusButton(status: string): string {
    if (status == 'PENDING') {
      return 'yellowButton';
    } else if (status == 'ACTIVE') {
      return 'greenButton';
    } else return 'redButton';
  }

  logout(): void {
    this.authService.logout().subscribe({
      next: () => {
        this.storageService.logout();
        this.router.navigate(['/login']);
      },
    });
  }

  ngOnDestroy(): void {
    if (this.usersSubscription) this.usersSubscription.unsubscribe();
  }
}
