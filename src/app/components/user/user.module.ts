import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ListUserComponent } from './list-user/list-user.component';
import { AvatarModule } from 'src/app/components/avatar/avatar.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileUserComponent } from './profile-user/profile-user.component';
import { RoleComponent } from './role/role.component';


@NgModule({
  declarations: [
    CreateUserComponent,
    EditUserComponent,
    ListUserComponent,
    DeleteUserComponent,
    ChangePasswordComponent,
    ProfileUserComponent,
    RoleComponent
  ],
  imports: [
    AvatarModule,
    CommonModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    CreateUserComponent,
    EditUserComponent,
    ListUserComponent,
    DeleteUserComponent,
    ChangePasswordComponent
  ]
})
export class UserModule { }
