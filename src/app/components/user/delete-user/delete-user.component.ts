import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { AlertComponent } from 'src/app/shared/alert/alert.component';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss'],
})
export class DeleteUserComponent {
  subscription: Subscription;
  username: string;
  id: number;
  dialogConfig: MatDialogConfig = new MatDialogConfig();
  errorMessage = '';

  constructor(
    private userService: UserService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<DeleteUserComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.id = data.id;
    this.username = data.username;
  }

  deleteUser(id:number): void {
    this.subscription = this.userService.deleteUser(id).subscribe({
      error: err => {
        this.errorMessage = err.error.message;
        this.showMessage(this.errorMessage);
      }
    });
    this.dialogRef.close(true);
    this.subscription.unsubscribe();
  }

  showMessage(message: string):void {
    this.dialogConfig.disableClose = false;
    this.dialogConfig.enterAnimationDuration = '500ms';
    this.dialogConfig.exitAnimationDuration = '500ms';
    this.dialogConfig.data = {
      messageAlert: message
    };

    this.dialog.open(AlertComponent, this.dialogConfig);
  }

  close(): void {
    this.dialogRef.close(false);
  }

}
