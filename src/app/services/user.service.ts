import { StorageService } from './storage.service';
import { IUser } from './../interface/IUser';
import { User } from './../model/user';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.storageService.getUser().token,
    }),
  };

  private apiURL: string = environment.API_URL;
  private usersSubject = new BehaviorSubject<IUser[]>([]);
  users$ = this.usersSubject.asObservable();
  params: { currentPassword: string; newPassword: string };
  errorMessage = '';

  constructor(
    private http: HttpClient,
    private storageService: StorageService,
    private router: Router
  ) {}

  getUserBoard(): Observable<any> {
    return this.http.get(`${this.apiURL}/currentUser`, {
      responseType: 'text',
    });
  }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(
      `${this.apiURL}/user/save`,
      user,
      this.httpOptions
    );
  }

  uploadAvatar(user: User): Observable<any> {
    return this.http.post<User>(
      `${this.apiURL}/user/uploadAvatar/${user.id}`,
      user.avatar.avatar220,
      this.httpOptions
    );
  }

  listAll(): void {
    this.http
      .get<IUser[]>(`${this.apiURL}/user/listAll`, this.httpOptions)
      .subscribe({ next: (users) => {
        let usersTemp = this.usersSubject.getValue();
        usersTemp = usersTemp.concat(users);
        this.usersSubject.next(users);
      },
      error: (err) => {
        this.errorMessage = err.status + err.statusText;
        if (err.status == '401') {
          this.storageService.logout();
          this.router.navigate(['/login']);
        }
      }
      });
  }

  editUser(user: User): Observable<User> {
    return this.http.post<User>(
      `${this.apiURL}/user/editUser/`,
      user,
      this.httpOptions
    );
  }

  findUserById(id: number): Observable<User> {
    return this.http.get<User>(
      `${this.apiURL}/user/findUserById/${id}`,
      this.httpOptions
    );
  }

  changePassword(
    id: number,
    currentPassword: string,
    newPassword: string
  ): Observable<any> {
    this.params = {
      currentPassword: currentPassword,
      newPassword: newPassword,
    };
    return this.http.post(
      `${this.apiURL}/user/changePassword/${id}`,
      this.params,
      this.httpOptions
    );
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete(
      `${this.apiURL}/user/deleteUser/${id}`,
      this.httpOptions
    );
  }
}
