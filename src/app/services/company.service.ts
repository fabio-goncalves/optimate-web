import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ICompany } from '../interface/ICompany';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.storageService.getUser().token,
    }),
  };

  private apiURL: string = environment.API_URL;
  private companyListSubject = new BehaviorSubject<ICompany[]>([]);
  companyList$ = this.companyListSubject.asObservable();

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  listAll(): Observable<ICompany[]> {
    return this.http
      .get<ICompany[]>(`${this.apiURL}/company/list/listAll`, this.httpOptions);
  }
}
