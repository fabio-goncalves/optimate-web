import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { IRole } from '../interface/IRole';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RoleService {
  private readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.storageService.getUser().token,
    }),
  };

  private apiURL: string = environment.API_URL;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  listAll(): Observable<IRole[]> {
    return this.http.get<IRole[]>(
      `${this.apiURL}/role/listAll`,
      this.httpOptions
    );
  }

  addRole(role: string): Observable<any> {
    return this.http.post<any>(
      `${this.apiURL}/user/uploadAvatar`,
      role,
      this.httpOptions
    );
  }
}
