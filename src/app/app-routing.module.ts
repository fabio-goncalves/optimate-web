import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MainShellComponent } from './shared/main-shell/main-shell.component';
import { LoginComponent } from './components/login/login.component';
import { CreateUserComponent } from './components/user/create-user/create-user.component';
import { ListUserComponent } from './components/user/list-user/list-user.component';
import { DeleteUserComponent } from './components/user/delete-user/delete-user.component';
import { ViewUserComponent } from './components/user/view-user/view-user.component';
import { EditUserComponent } from './components/user/edit-user/edit-user.component';
import { AuthGuard } from './helpers/auth.guard';
import { ChangePasswordComponent } from './components/user/change-password/change-password.component';
import { ProfileUserComponent } from './components/user/profile-user/profile-user.component';
import { RoleComponent } from './components/user/role/role.component';

const routes: Routes = [
  {
    path: '',
    component: MainShellComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'create-user',
        component: CreateUserComponent,
      },
      {
        path: 'profile-user',
        component: ProfileUserComponent,
      },
      {
        path: 'list-user',
        component: ListUserComponent,
      },
      {
        path: 'delete-user',
        component: DeleteUserComponent,
      },
      {
        path: 'view-user',
        component: ViewUserComponent,
      },
      {
        path: 'edit-user',
        component: EditUserComponent,
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent,
      },
      {
        path: 'role',
        component: RoleComponent,
      },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
    ],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
