export interface ICompany {
  id?: number;
  acronym: string;
  name: string;
  cnpj: string;
  razaoSocial: string;
  inscricaoEstadual: string;
  inscricaoMunicipal: string;
  phoneNumber: string;
  cellPhone: string;
  email: string;
  isActive: boolean;
  businessArea: BusinessArea;
}

export interface BusinessArea {
  id?: number;
  name: string;
  description: string;
}
