import { Avatar } from './IAvatar';
import { PersonalInformation } from './IPersonalInformation';
import { ICompany } from './ICompany';

export interface IUser {
  id?: number;
  pesonalInformation: PersonalInformation;
  username: string;
  password: string;
  receiveEmails: boolean;
  status: string;
  roles?: string[];
  avatar: Avatar;
  companyList: ICompany[];
}
