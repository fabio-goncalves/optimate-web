export interface PersonalInformation {
  id?: number;
  firstName: string;
  lastName: string;
  email: string;
  activated: boolean;
}
